<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Share;
use App\Form\ShareType;
use Symfony\Component\HttpFoundation\Request;
use App\Exception\ShareException;
use App\Service\ShareService;
use Symfony\Component\Form\FormError;

/**
 * @Route("/share")
 */
class ShareController extends Controller
{
    /**
     * @Route("", name="share_index")
     */
    public function index(Request $request, ShareService $shareService)
    {
        $form = $this->createForm(ShareType::class, new Share());

        $session = $request->getSession();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $share = $form->getData();

            try {
                $shareService->save($share);
                $shareService->sendNotificationEmail($share->senderName, $share->recipientEmail);

                $session->getFlashBag()->add('share-success', sprintf('You\'ve successfully shared the deal with %s', $share->recipientName));

                return $this->redirectToRoute('share_index');
            } catch (ShareException $e){
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('Share/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
