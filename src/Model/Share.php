<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Share
{
  /**
   * @Assert\NotBlank()
   */
  public $senderName;

  /**
   * @Assert\NotBlank()
   */
  public $recipientName;

  /**
   * @Assert\NotBlank()
   */
  public $recipientEmail;
}
