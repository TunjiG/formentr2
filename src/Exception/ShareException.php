<?php

namespace App\Exception;

use RuntimeException;

class ShareException extends RuntimeException
{
}
