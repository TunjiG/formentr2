<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type as SymfonyFormType;
use App\Model\Share;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShareType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'senderName',
                SymfonyFormType\TextType::class,
                [
                  'label' => 'Your name',
                  'required' => true,
                  'attr' => ['class' => 'input', 'placeholder' => 'Your name'],
                ]
            )
            ->add(
                'recipientName',
                SymfonyFormType\TextType::class,
                [
                  'label' => 'Friend\'s name',
                  'required' => true,
                  'attr' => ['class' => 'input', 'placeholder' => 'Friend\'s name'],
                ]
            )
            ->add(
                'recipientEmail',
                SymfonyFormType\EmailType::class,
                [
                  'label' => 'Friend\'s email',
                  'required' => true,
                  'attr' => ['class' => 'input', 'placeholder' => 'Friend\'s email'],
                ]
            )
            ->add(
                'submit',
                SymfonyFormType\SubmitType::class,
                [
                    'label' => 'submit',
                    'attr' => ['class' => 'btn-primary']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Share::class,
        ));
    }
}
