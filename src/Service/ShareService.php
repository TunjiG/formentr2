<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Swift_Message;
use App\Model\Share as ShareModel;
use App\Entity\Share as ShareEntity;
use App\Exception\ShareException;
use Exception;

class ShareService
{
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, Swift_Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function save(ShareModel $shareModel)
    {
        try {
            $shareEntity = (
                new ShareEntity()
            )
            ->setSenderName($shareModel->senderName)
            ->setRecipientName($shareModel->recipientName)
            ->setRecipientEmail($shareModel->recipientEmail);

            $this->entityManager->persist($shareEntity);
            $this->entityManager->flush();
        } catch (Exception $e) {
            throw new ShareException('Could not save the share.');
        }
    }

    public function sendNotificationEmail($senderName, $recipientEmail)
    {
        $message = (new Swift_Message('A deal has been shared with you!'))
            ->setFrom('admin@example.com')
            ->setTo($recipientEmail)
            ->addPart(
                sprintf('%s has shared a deal with you.', $senderName)
            );

        if (!$this->mailer->send($message)) {
            throw new ShareException(sprinft('Could not send an email to %s', $recipientEmail));
        }
    }
}
