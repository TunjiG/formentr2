<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="share")
 */
class Share
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $senderName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $recipientName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $recipientEmail;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Recipient Name
     *
     * @return mixed
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }

    /**
     * Set the value of Recipient Name
     *
     * @param mixed recipientName
     *
     * @return self
     */
    public function setRecipientName($recipientName)
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    /**
     * Get the value of Recipient Email
     *
     * @return string
     */
    public function getRecipientEmail()
    {
        return $this->recipientEmail;
    }

    /**
     * Set the value of Recipient Email
     *
     * @param string recipientEmail
     *
     * @return self
     */
    public function setRecipientEmail($recipientEmail)
    {
        $this->recipientEmail = $recipientEmail;

        return $this;
    }


    /**
     * Get the value of Sender Name
     *
     * @return mixed
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Set the value of Sender Name
     *
     * @param mixed senderName
     *
     * @return self
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;

        return $this;
    }

}
