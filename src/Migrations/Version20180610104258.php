<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180610104258 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__share AS SELECT id, name, recipient_name, recipient_email FROM share');
        $this->addSql('DROP TABLE share');
        $this->addSql('CREATE TABLE share (id INTEGER NOT NULL, recipient_name VARCHAR(100) NOT NULL COLLATE BINARY, recipient_email VARCHAR(255) NOT NULL COLLATE BINARY, sender_name VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO share (id, sender_name, recipient_name, recipient_email) SELECT id, name, recipient_name, recipient_email FROM __temp__share');
        $this->addSql('DROP TABLE __temp__share');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__share AS SELECT id, sender_name, recipient_name, recipient_email FROM share');
        $this->addSql('DROP TABLE share');
        $this->addSql('CREATE TABLE share (id INTEGER NOT NULL, recipient_name VARCHAR(100) NOT NULL, recipient_email VARCHAR(255) NOT NULL, name VARCHAR(100) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO share (id, name, recipient_name, recipient_email) SELECT id, sender_name, recipient_name, recipient_email FROM __temp__share');
        $this->addSql('DROP TABLE __temp__share');
    }
}
