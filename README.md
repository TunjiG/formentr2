1. Clone the repository
2. Copy `.env.dist` to `.env`
3. Modify the `DATABASE_DRIVER`, `DATABASE_URL`, and `MAILER_URL` paramaters inside the `.env` file
4. Execute `composer install`
5. Execute `bin/console doctrine:schema:create`
6. Execute `bin/console server:run`
7. Navigate to `http://localhost:8000/share` in your local browser